Default subject：{TRIGGER.STATUS}: {TRIGGER.NAME}
 
Trigger host:{HOSTNAME}
Trigger ip:{HOST.IP}
Trigger time:{EVENT.DATE}:{EVENT.TIME}
Trigger: {TRIGGER.NAME}
Trigger status: {TRIGGER.STATUS}
Trigger severity: {TRIGGER.SEVERITY}
Trigger URL: {TRIGGER.URL}
 
Item values:
{ITEM.NAME1} ({HOST.NAME1}:{ITEM.KEY1}): {ITEM.VALUE1}
{ITEM.NAME2} ({HOST.NAME2}:{ITEM.KEY2}): {ITEM.VALUE2}
 
Original event ID: {EVENT.ID}